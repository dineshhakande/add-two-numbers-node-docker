# add-two-number-node

## Setting it up

1. Install nodejs
2. Clone this repository into a folder
3. Run "npm install" in your root folder
4. Run "npm start" to ensure the program runs in localhost
5. visit http://localhost:3000/ 
6. check the output as Hello World!

if the output is Hello World!, it means that your code works. Now let's publish it to docker

Note this was build using windows settings of docker. 

Inorder to run it on windown you need Hyper-V Virtual Machine Management service to run.

If this is not visible in services.msc ensure it to add the program feature from Add or remove programs.

Sample workspace for [Dockerizing a Node.js web app](https://nodejs.org/de/docs/guides/nodejs-docker-webapp/)

## Build NodeJS Docker Webapp (container)

> $ docker build -t <your_username>/add-two-number-node .

## list down your image listed by docker

> $ docker images

## Start the dockerized Webapp

Running your image with -d runs the container in detached mode, leaving the container running in the background. The -p flag redirects a public port to a private port inside the container.

Run the webapp:

> $ docker run -p 49160:3000 <your_username>/add-two-number-node

## get the container id

> $ docker ps

## Print app output

> $ docker logs <container_id>

## go inside the container use the exec command:

> $ docker exec -it <container id> /bin/bash

## TEST your app

### get the port of your app that Docker mapped

> $ docker ps

### call your app using curl

> $ curl -i localhost:49160

