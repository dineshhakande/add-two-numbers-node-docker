var express = require('express');
var app = express();

const fs = require('fs');
let rawdata = fs.readFileSync('input.json');
let inputJson = JSON.parse(rawdata);  

let output = {
  sum: 0
}

output.sum = inputJson.input[0].input_1.value.first_number + inputJson.input[0].input_2.value.second_number;

fs.writeFileSync('output.json', JSON.stringify(output)); 

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
